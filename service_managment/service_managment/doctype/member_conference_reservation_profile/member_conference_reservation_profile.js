// Copyright (c) 2019, Devienta and contributors
// For license information, please see license.txt

frappe.ui.form.on('Member Conference Reservation Profile', {
	refresh: function (frm) {
		if (cur_dialog) {
			cur_dialog.set_value('document_type', cur_frm.doctype);
			cur_dialog.set_value('member', cur_frm.docname);
		}
	}
});