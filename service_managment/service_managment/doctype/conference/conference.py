# -*- coding: utf-8 -*-
# Copyright (c) 2019, Devienta and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import  _
from frappe.utils import getdate
from frappe.model.document import Document

class Conference(Document):
	def validate(self):
		self.validate_conf_dates()

	def validate_conf_dates(self):
		if getdate(self.start_date) > getdate(self.end_date):
			frappe.throw(_("Conference musy begins before it ends."))