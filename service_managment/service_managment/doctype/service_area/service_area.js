// Copyright (c) 2018, Devienta and contributors
// For license information, please see license.txt

frappe.ui.form.on('Service Area', {
	refresh: function(frm) {

	},
	service_team: function(frm) {
		console.log(frm);
		if (frm.doc.service_team) {
			frappe.call({
				method: "service_managment.service_managment.doctype.service_team.service_team.get_team_members",
				args: {
					team: frm.doc.service_team
				},
				callback: function (r) {
					if(r && r.message) {
						var template = `<div>
						 {%for servant in servants %} 
						 	<li> {{ servant }} </li> 
						 {% endfor %}
						 </div>
						`;
						frm.set_df_property('team_servants', 'options', frappe.render(template, {
								servants: r.message
						})); 
						frm.refresh_field('team_servants');
					}
				}
			})
		}
	}
});
