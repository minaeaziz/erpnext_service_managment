# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Service Managment",
			"color": "#7578f6",
			"icon": "octicon octicon-organization",
			"type": "module",
			"label": _("Service Managment")
		}
	]
